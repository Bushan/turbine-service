# # Turbine Service

This is sample service for aggregating hystrix streams.

## Running

Running from CLI
 
	$ java -jar turbine-service-0.0.1.jar

or, Running main method class 'TurbineServiceApplication.java' from IDE or CLI

Its configured to run on port '8989' 

This get auto register with Eureka Discovery service to serve the Eureka API from "/eureka".

URL for Hystrix dashboard
 
	e.g. http://localhost:8989/turbine.stream?cluster=POC2
	
## Docker

Running in Docker container

	$ docker run -p 8989:8989 -e "EUREKA_SERVER_HOST=<server IP>" -e "EUREKA_SERVER_PORT=<server port e.g. 8761>" -t <image-prefix>/turbine-service:<image-tag>


Its configured to build docker image and push to private nexus repository using maven plugin.
	
	mvn clean -X package docker:build -DpushImage

Maven properties

	docker.image.prefix=<nexus host>:18079
	docker.registry.id=<Docker Repository name>
	registry.url=<registry url e.g. https://192.168.1.98:8449/repository/myDockerRepo/ >
	
